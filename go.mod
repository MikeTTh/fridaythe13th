module gitlab.com/MikeTTh/fridaythe13th

go 1.15

replace gitlab.com/MikeTTh/fridaythe13th => ./

require (
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	golang.org/x/net v0.0.0-20200904194848-62affa334b73
	golang.org/x/text v0.3.3 // indirect
)
