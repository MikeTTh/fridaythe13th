package main

import (
	"fmt"
	"gitlab.com/MikeTTh/fridaythe13th/common"
	"gitlab.com/MikeTTh/fridaythe13th/save"
	"gitlab.com/MikeTTh/fridaythe13th/webstuff"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"sync"
	"time"
)

func main() {
	data := &save.Save{}
	data.Load()
	defer data.Save()
	go func(d *save.Save) {
		for true {
			data.Save()
			time.Sleep(time.Minute)
		}
	}(data)
	webstuff.Root = &webstuff.RootStr{
		Data:  data,
		Chals: &webstuff.Challenges,
	}
	for _, s := range data.SolvedChals {
		webstuff.MarkDone(s)
	}
	for _, s := range data.HintsUsed {
		for i, c := range webstuff.Challenges {
			if c.Name == s {
				webstuff.Challenges[i].HintUsed = true
			}
		}
	}

	_ = os.Remove("/tmp/blint.sock")
	l, err := net.Listen("unix", "/tmp/blint.sock")
	if err != nil {
		panic(err)
	}
	defer l.Close()
	_ = os.Chmod("/tmp/blint.sock", 0666)
	rpcHandler := &common.RpcHandler{Data: data}
	err = rpc.Register(rpcHandler)
	if err != nil {
		_ = fmt.Errorf("\nBekagiltam: \n", err)
	}
	go rpc.Accept(l)

	for !(data.RpcDone) {
		time.Sleep(500 * time.Millisecond)
	}

	fmt.Printf("RPC section done.\n")
	mux := http.NewServeMux()
	mux.HandleFunc("/", webstuff.HandleRoot)
	server := &http.Server{
		Handler: mux,
		Addr:    ":8080",
	}
	fmt.Printf("listening on %s\n", server.Addr)
	go func() {
		for true {
			err = server.ListenAndServe()
			_ = fmt.Errorf("\nBekagiltam: \n", err)
		}
	}()
	var asd sync.WaitGroup
	asd.Add(1)
	asd.Wait()
}
