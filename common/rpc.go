package common

import (
	"gitlab.com/MikeTTh/fridaythe13th/save"
	"os/exec"
	"strings"
)

type RpcResponse struct {
	Message string
	Done    bool
	Prompt  string
}

type RpcRequest struct {
	Command string
}

type RpcHandler struct {
	Data *save.Save
}

type rpcState int

const (
	def rpcState = iota
	figlet
	flag
)

var Killable = false

var state rpcState = def

func sanitize(str *string) {
	banned := []string{
		"rm",
		"reboot",
		"shutdown",
		"kill",
		"sh",
		"python",
		"su",
	}
	for _, ban := range banned {
		if strings.Contains(*str, ban) {
			*str = "figlet 'no, not yet...'"
		}
	}
}

func (h *RpcHandler) Execute(req RpcRequest, res *RpcResponse) (err error) {
	res.Done = false
	switch state {
	case def:
		res.Prompt = "[SecureShell]$ "
		switch req.Command {
		case "credits":
			res.Message = `Nem is tudom ki csinálhatta? 🤔
(na jó, Cssribi is néha bele "display: flex"-elt)`
			break
		case "help":
			res.Message = `help: list commands
credits: show credits
figlet: a legeslegbiztonságosabb figlet a világon
hint: hintet ad (számolom őket 😉)
flag: enter flag`
			break
		case "figlet":
			state = figlet
			res.Message = "Nagyon \"biztonságos\" figlet hívások jönnek 😉"
			res.Prompt = "[SecureShell]$ figlet "
			break
		case "hint":
			res.Message = `A figlet parancs az egy nagyon "biztonságos" figlet hívást csinál
ami véletlenül sem egy bashen keresztül történik 😉
Ja és btw számolom a hintjeidet 😉`
			h.Data.HintsUsed = append(h.Data.HintsUsed, "RPC")
			res.Prompt = "[SecureShell 🙄]$ "
			break
		case "flag":
			res.Message = "Én hiszek benned"
			res.Prompt = "[SecureShell]$ flag="
			state = flag
			break
		default:
			res.Message = "For a list of commands, type help."
			break
		}
		break
	case flag:
		switch req.Command {
		case "easylife":
			h.Data.RpcDone = true
			res.Done = true
			res.Message = `Ügyes.
https://ctf.sch.mikesweb.site`
			break
		default:
			res.Message = `Sajnos nem nyert`
			h.Data.TriedFlags = append(h.Data.TriedFlags, req.Command)
			res.Prompt = "[SecureShell]$ "
			state = def
			break
		}
		break
	case figlet:
		state = def
		bashArg := "figlet " + req.Command
		if !Killable {
			sanitize(&bashArg)
		}
		cmd := exec.Command("bash", "-c", bashArg)
		out, _ := cmd.Output()
		res.Message = string(out)
		res.Prompt = "[SecureShell]$ "
		break
	default:
		res.Message = "For a list of commands, type help."
		break
	}
	return
}
