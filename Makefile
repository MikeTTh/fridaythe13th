all: go go_console docker

go:
	go build -o app

go_console:
	bash -c "cd console; make"

docker:
	docker build -t "ctf:latest" .
