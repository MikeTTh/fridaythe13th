package webstuff

import (
	"encoding/json"
	"gitlab.com/MikeTTh/fridaythe13th/save"
	"html/template"
	"math/rand"
	"net/http"
)

var temp *template.Template

type Challenge struct {
	Name        string
	Description string
	Path        string
	Enabled     bool
	Done        bool
	Flag        string
	Hint        string
	HintUsed    bool
	Handler     http.HandlerFunc
}

type RootStr struct {
	Data  *save.Save
	Chals *[]Challenge
}

func (r *RootStr) All() int {
	return len(*r.Chals)
}

func (r *RootStr) Hints() int {
	h := 0
	for _, c := range *Root.Chals {
		if c.HintUsed {
			h++
		}
	}
	return h
}

func (r *RootStr) Solved() int {
	s := 0
	for _, c := range *Root.Chals {
		if c.Done {
			s++
		}
	}
	return s
}

func (r *RootStr) Progress() float64 {
	return float64(r.Solved()) / float64(r.All()) * 100
}

var solvedMsgs = []string{"Good job!", "Ügyes", "Nice", "Cool-cool"}

func (c *Challenge) SolvedMsg() string {
	ran := rand.Intn(len(solvedMsgs))
	return solvedMsgs[ran]
}

var Root *RootStr

func HandleRoot(w http.ResponseWriter, r *http.Request) {
	var err error
	if temp == nil {
		temp, err = template.ParseFiles("webstuff/root.html")
	}
	if err != nil {
		panic(err)
	}

	switch r.URL.Path {
	case "/", "/index.html":
		_ = temp.Execute(w, Root)
		break
	case "/favicon.ico":
		http.ServeFile(w, r, "webstuff/favicon.ico")
		break
	case "/style.css":
		http.ServeFile(w, r, "webstuff/style.css")
		break
	case "/getHint":
		q := r.URL.Query()
		for qname, _ := range q {
			for i, q := range Challenges {
				if qname == q.Name && !q.HintUsed {
					Challenges[i].HintUsed = true
					Root.Data.HintsUsed = append(Root.Data.HintsUsed, qname)
				}
			}
		}
		http.Redirect(w, r, "/", http.StatusSeeOther)
		break
	case "/nothingtoseehere":
		dat, _ := json.Marshal(Root.Data)
		_, _ = w.Write(dat)
		break
	case "/submitFlag":
		q := r.URL.Query()
		for qname, val := range q {
			for _, q := range Challenges {
				if qname == q.Name {
					if q.Flag == val[0] {
						if q.Done {
							http.Redirect(w, r, "/", http.StatusSeeOther)
							return
						} else {
							MarkDone(qname)
							http.ServeFile(w, r, "webstuff/chaldone.html")
							return
						}
						return
					} else {
						Root.Data.TriedFlags = append(Root.Data.TriedFlags, val[0])
					}
				}
			}
		}
		http.ServeFile(w, r, "webstuff/nemnyert.html")
		break
	default:
		handled := false
		i := 0
		for i = len(r.URL.Path) - 1; r.URL.Path[i] != '/'; i-- {
		}
		path := r.URL.Path[:i]
		for _, c := range *Root.Chals {
			if path == c.Path || r.URL.Path == c.Path {
				if c.Enabled {
					handled = true
					c.Handler(w, r)
				}
			}
		}
		if !handled {
			w.WriteHeader(http.StatusNotFound)
			http.ServeFile(w, r, "webstuff/404.html")
		}
		break
	}
}
