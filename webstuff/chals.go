package webstuff

/*
typedef struct login {
    char jelszo[20];
    char helyes[20];
	char padding[200];
} login;

login asd = {"", "nagyonbonyi"};

int osszehasonlit (char* pass){
    int i = 0;
    for(i = 0; pass[i] != '\0'; i++){
        asd.jelszo[i] = pass[i];
    }
    asd.jelszo[i] = '\0';
    for (int i = 0; i < 20; ++i) {
        if(asd.jelszo[i] != asd.helyes[i])
            return 0;
    }
    return 1;
}
*/
import "C"

import (
	"gitlab.com/MikeTTh/fridaythe13th/common"
	"net/http"
	"os"
	"time"
)

var fs = http.FileServer(http.Dir("webstuff"))

var Challenges = []Challenge{
	{
		Name:        "LoginPage",
		Description: "A very sophisticated and secure login page",
		Hint:        "Client side verification is fun",
		Path:        "/first",
		Flag:        "jsislove",
		Enabled:     true,
		Handler: func(w http.ResponseWriter, r *http.Request) {
			if r.URL.Path == "/first/good" {
				_, _ = w.Write([]byte("flag=jsislove"))
				return
			}
			fs.ServeHTTP(w, r)
		},
	},
	{
		Name:        "I'm hiding",
		Description: `Sometimes the simplest solution is the easiest one`,
		Hint:        "Can you hide behind a bush?",
		Path:        "/bushy",
		Flag:        "911",
		Handler: func(writer http.ResponseWriter, request *http.Request) {
			fs.ServeHTTP(writer, request)
		},
	},
	{
		Name:        "Buffering...",
		Description: "What did we learn about C stuff?",
		Hint:        "Buffer overfloooooow",
		Path:        "/buf",
		Flag:        "h4xorz",
		Handler: func(w http.ResponseWriter, r *http.Request) {
			if r.URL.Path == "/buf/login" {
				q := r.URL.Query()
				for qname, val := range q {
					if qname == "pass" {
						s := val[0]
						if len(s) > 42 {
							s = s[:41]
						}
						if C.osszehasonlit(C.CString(s)) == 1 {
							_, _ = w.Write([]byte("flag=h4xorz"))
							return
						} else {
							_, _ = w.Write([]byte("incorrect password"))
							return
						}
					}
				}
				_, _ = w.Write([]byte("invalid query"))
				return
			} else {
				fs.ServeHTTP(w, r)
			}
		},
	},
	{
		Name:        "Snek",
		Path:        "/snek",
		Description: "Cool game, right?",
		Hint:        "The dev tools are there at your disposal 😉",
		Flag:        "asnakeisalwaysfun",
		Handler: func(writer http.ResponseWriter, request *http.Request) {
			fs.ServeHTTP(writer, request)
		},
	},
	{
		Name:        "Hmmm...",
		Description: "Hmmm...",
		Path:        "/hmmm",
		Hint:        "metadata is cool",
		Flag:        "gimmemyflag",
		Handler: func(writer http.ResponseWriter, request *http.Request) {
			fs.ServeHTTP(writer, request)
		},
	},
	{
		Name:        "That time of the year",
		Description: "I think there is .mor.e to .se.e here",
		Hint:        "Look at the blinkies",
		Path:        "/christmas",
		Flag:        "jinglebells",
		Handler: func(writer http.ResponseWriter, request *http.Request) {
			fs.ServeHTTP(writer, request)
		},
	},
	{
		Name: "Final boss",
		Description: `Take down the server!
Do it, I dare you!`,
		Hint: "Sometimes you have to go back to go forward.",
		Flag: "",
		Path: "/clock",
		Handler: func(writer http.ResponseWriter, request *http.Request) {
			if !common.Killable {
				go func() {
					run := true
					for run {
						if _, e := os.Stat("webstuff/root.html"); e != nil {
							MarkDone("Final boss")
							time.Sleep(time.Second)
							run = false
						}
					}
				}()
				common.Killable = true
			}
			_, _ = writer.Write([]byte("The server says the time is: "))
			t := time.Now()
			_, _ = writer.Write([]byte(t.Format(time.RFC850)))
			_, _ = writer.Write([]byte("\nThis is just to check if the server is lagging behind"))
			_, _ = writer.Write([]byte("\nAlso, this might help: https://gitlab.com/MikeTTh/fridaythe13th"))
		},
	},
}

func MarkDone(chalName string) {
	found := false
	for i, c := range *Root.Chals {
		if c.Name == chalName {
			(*Root.Chals)[i].Done = true
			Root.Data.SolvedChals = append(Root.Data.SolvedChals, chalName)
			found = true
		} else if found {
			(*Root.Chals)[i].Enabled = true
			break
		}
	}
}
