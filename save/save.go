package save

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"time"
)

type Save struct {
	RpcDone     bool
	WebStage    int
	TriedFlags  []string
	SolvedChals []string
	HintsUsed   []string
}

func (s *Save) Save() {
	byt, err := json.Marshal(s)
	if err != nil {
		fmt.Println("Save failed, trying again in 3 secs...")
		time.Sleep(3 * time.Second)
		s.Save()
		return
	}
	err = ioutil.WriteFile("save.json", byt, os.ModePerm)
	if err != nil {
		fmt.Println("Save failed, trying again in 3 secs...")
		time.Sleep(3 * time.Second)
		s.Save()
		return
	}
	fmt.Println("Saved...")
}

func (s *Save) Load() {
	byt, err := ioutil.ReadFile("save.json")
	if err != nil {
		fmt.Println("File reading failed.")
		return
	}
	err = json.Unmarshal(byt, s)
	if err != nil {
		fmt.Println("File reading failed.")
		return
	}
}
