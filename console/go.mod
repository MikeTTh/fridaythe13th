module gitlab.com/MikeTTh/fridaythe13th/console

go 1.15

replace gitlab.com/MikeTTh/fridaythe13th/console => ./
replace gitlab.com/MikeTTh/fridaythe13th => ../

require gitlab.com/MikeTTh/fridaythe13th v0.0.0-20191212222919-ef4d8114d4c5
