package main

import (
	"bufio"
	"fmt"
	"gitlab.com/MikeTTh/fridaythe13th/common"
	"net/rpc"
	"os"
	"time"
)

func main() {
	connStr := "Csatlakozás...\n"
	for _, c := range connStr {
		fmt.Print(string(c))
		time.Sleep(100 * time.Millisecond)
	}
	c, err := rpc.Dial("unix", "/tmp/blint.sock")
	if err != nil {
		panic(err)
	}
	defer c.Close()
	in := &common.RpcRequest{}
	out := &common.RpcResponse{}
	read := bufio.NewReader(os.Stdin)
	_ = c.Call("RpcHandler.Execute", in, &out)
	fmt.Println("Pfuh. sikerült csatlakozni, még él a szerver 👌")
	fmt.Println("Have fun! 🐘💨")
	for true {
		fmt.Print(out.Prompt)
		text, _ := read.ReadString('\n')
		if len(text) == 0 {
			return
		}
		text = text[:len(text)-1]
		in.Command = text
		_ = c.Call("RpcHandler.Execute", in, &out)
		fmt.Println(out.Message)
		if out.Done {
			return
		}
	}
}
