FROM ubuntu:latest

WORKDIR "/app"

ADD "." "/app/src"
ADD "entrypoint.sh" "./"

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y golang make figlet openssh-server sudo && \
    mkdir /run/sshd && \
    cd /app/src && \
    go build -o /app/src/app && \
    cd /app/src/console && \
    go build -o /app/console && \
    useradd -m -s /bin/bash user && \
    printf "password\npassword" | passwd user && \
    echo "exec /app/console" >> /home/user/.bashrc && \
    rm -f /app/src/save.json


RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

EXPOSE 22 8080

ENTRYPOINT ["/app/entrypoint.sh"]